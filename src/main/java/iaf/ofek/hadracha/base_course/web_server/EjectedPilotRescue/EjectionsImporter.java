package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Utilities.Action;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;
    @Value("${ejections.namespace}")
    public String NAMESPACE;

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final RestTemplate restTemplate;
    private final CrudDataBase dataBase;
    private final ListOperations listOperations;
    private static final Double SHIFT_NORTH = 1.7;

    public EjectionsImporter(RestTemplateBuilder restTemplateBuilder, CrudDataBase dataBase, ListOperations listOperations) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperations = listOperations;
        executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);
    }

    private void updateEjections() {
        try {
            ResponseEntity<List<EjectedPilotInfo>> responseEntity = getEjectionsResponse();
            List<EjectedPilotInfo> ejectionsFromServer = responseEntity.getBody();
            this.shiftEjectionsNorth(ejectionsFromServer);

            List<EjectedPilotInfo> ejectionsFromDB = dataBase.getAllOfType(EjectedPilotInfo.class);
            List<EjectedPilotInfo> addedEjections = removeOrAddEjections(ejectionsFromServer, ejectionsFromDB, Action.ADD);
            List<EjectedPilotInfo> removedEjections = removeOrAddEjections(ejectionsFromServer, ejectionsFromDB, Action.REMOVE);

            this.updateEjectionsInDB(addedEjections, removedEjections);
        } catch (RestClientException error) {
            System.out.println("Error: " + error.getMessage());
            error.printStackTrace();
        }
    }

    private void updateEjectionsInDB(List<EjectedPilotInfo> addedEjections, List<EjectedPilotInfo> removedEjections) {
        addedEjections.forEach(dataBase::create);
        removedEjections.stream().map(EjectedPilotInfo::getId).forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
    }

    private void shiftEjectionsNorth(List<EjectedPilotInfo> ejections) {
        if (ejections != null) {
            for(EjectedPilotInfo ejectedPilotInfo: ejections) {
                ejectedPilotInfo.getCoordinates().lat += SHIFT_NORTH;
            }
        }
    }

    private List<EjectedPilotInfo> removeOrAddEjections(List<EjectedPilotInfo> updatedEjections,
                                                          List<EjectedPilotInfo> previousEjections,
                                                          Action action) {
        switch(action) {
            case ADD: {
                return listOperations.subtract(updatedEjections, previousEjections);
            }
            case REMOVE: {
                return listOperations.subtract(previousEjections, updatedEjections);
            }
            default: return null;
        }
    }

    @NotNull
    private ResponseEntity<List<EjectedPilotInfo>> getEjectionsResponse() {
        return restTemplate.exchange(
                EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                });
    }
}
