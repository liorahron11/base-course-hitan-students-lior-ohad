package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("ejectedPilotRescue/")
public class EjectedPilotRescueRestController {

    @Autowired private CrudDataBase dataBase;
    @Autowired private AirplanesAllocationManager airplanesAllocationManager;

    @GetMapping("infos")
    public List<EjectedPilotInfo> getPilots() {
        return dataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("takeResponsibility")
    public EjectedPilotInfo getResponsibility(
            @RequestParam("ejectionId") int ejectionId,
            @CookieValue(value = "client-id", defaultValue = "") String clientId) {

        EjectedPilotInfo ejectedPilot = dataBase.getByID(ejectionId, EjectedPilotInfo.class);
        if (ejectedPilot.getRescuedBy() == null) {
            ejectedPilot.setRescuedBy(clientId);;
            dataBase.update(ejectedPilot);
            airplanesAllocationManager.allocateAirplanesForEjection(ejectedPilot, clientId);
        }

        return ejectedPilot;
    }
}
